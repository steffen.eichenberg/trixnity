workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_PIPELINE_SOURCE == "push"

stages:
  - website
  - prepare-build
  - build
  - test
  - publish-snapshot
  - publish

variables:
  GRADLE_OPTS: -Dorg.gradle.daemon=false
  GRADLE_USER_HOME: $CI_PROJECT_DIR/.gradle
  KONAN_DATA_DIR: $CI_PROJECT_DIR/.konan
  BUILD_LINUX_IMAGE_NAME: ${CI_REGISTRY_IMAGE}/trixnity-build-linux:latest

.cache-linux: &cache-linux
  - key: global-trixnity-cache-linux
    policy: pull-push
    paths:
      - $GRADLE_USER_HOME
      - $KONAN_DATA_DIR
  - key: "$CI_COMMIT_REF_NAME-linux"
    policy: push
    paths:
      - $GRADLE_USER_HOME
      - $KONAN_DATA_DIR

.pull-cache-linux: &pull-cache-linux
  - key: "$CI_COMMIT_REF_NAME-linux"
    fallback_keys: [ "global-trixnity-cache-linux" ]
    policy: pull
    paths:
      - $GRADLE_USER_HOME
      - $KONAN_DATA_DIR

.cache-mac: &cache-mac
  - key: global-trixnity-cache-mac
    policy: pull-push
    paths:
      - $GRADLE_USER_HOME
      - $KONAN_DATA_DIR
  - key: "$CI_COMMIT_REF_NAME-mac"
    policy: push
    paths:
      - $GRADLE_USER_HOME
      - $KONAN_DATA_DIR

.pull-cache-mac: &pull-cache-mac
  - key: "$CI_COMMIT_REF_NAME-mac"
    fallback_keys: [ "global-trixnity-cache-mac" ]
    policy: pull
    paths:
      - $GRADLE_USER_HOME
      - $KONAN_DATA_DIR

.docker-variables: &docker-variables
  DOCKER_HOST: tcp://docker:2375
  DOCKER_TLS_CERTDIR: ""
  DOCKER_DRIVER: overlay2

.artifact-reports: &artifact-reports
  name: reports
  when: always
  paths: [ "**/build/reports" ]
  reports:
    junit: "**/build/test-results/**/TEST-*.xml"

.if-merge-request: &if-merge-request
  - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME != $CI_DEFAULT_BRANCH
    when: never
  - if: $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME

.if-release: &if-release
  - if: '$CI_COMMIT_TAG =~ /^v\d+.\d+.\d+.*/'

.if-main: &if-main
  - if: '$CI_COMMIT_TAG =~ /^v\d+.\d+.\d+.*/'
    when: never
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

pages:
  stage: website
  image: node:lts
  rules:
    - *if-main
  script:
    - cd website
    - yarn install
    - yarn build
    - mv ./build ../public
  artifacts:
    paths:
      - public

build-trixnity-build-linux-image:
  stage: prepare-build
  tags: [ "saas-linux-small-amd64" ]
  image: docker:20
  services:
    - name: docker:dind
      command: [ "--tls=false" ]
  when: manual
  script:
    - docker build gitlab-ci/ -t $BUILD_LINUX_IMAGE_NAME
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker push $BUILD_LINUX_IMAGE_NAME

build:mac:
  stage: build
  tags: [ "trixnity-mac-internal" ]
  cache:
    - *cache-mac
  rules:
    - *if-merge-request
  script:
    - ./gradlew assemble --stacktrace

build:linux:
  stage: build
  tags: [ "saas-linux-large-amd64" ]
  image: $BUILD_LINUX_IMAGE_NAME
  cache:
    - *cache-linux
  rules:
    - *if-merge-request
  script:
    - ./gradlew assemble --stacktrace

test:mac:
  stage: test
  timeout: 1h
  needs: [ "build:mac" ]
  tags: [ "trixnity-mac-internal" ]
  cache:
    - *pull-cache-mac
  artifacts: *artifact-reports
  rules:
    - *if-merge-request
  # TODO iosArm64Test not created by gradle
  script:
    - ./gradlew
      iosSimulatorArm64Test
      iosX64Test
      macosArm64Test
      macosX64Test
      --stacktrace

test:linux:
  stage: test
  timeout: 1h
  needs: [ "build:linux" ]
  tags: [ "saas-linux-large-amd64" ]
  image: $BUILD_LINUX_IMAGE_NAME
  cache:
    - *pull-cache-linux
  artifacts: *artifact-reports
  rules:
    - *if-merge-request
  script:
    - ./gradlew
      jvmTest -x :trixnity-client:integration-tests:jvmTest
      testReleaseUnitTest
      jsTest
      linuxX64Test
      mingwX64Test
      --stacktrace

test:integration-test:
  stage: test
  timeout: 30m
  needs: [ "build:linux" ]
  tags: [ "saas-linux-medium-amd64" ]
  image: $BUILD_LINUX_IMAGE_NAME
  services:
    - name: docker:dind
      command: [ "--tls=false" ]
  cache:
    - *pull-cache-linux
  variables: *docker-variables
  artifacts: *artifact-reports
  rules:
    - *if-merge-request
  script:
    - ./gradlew :trixnity-client:integration-tests:jvmTest --stacktrace

publish-snapshot:mac:
  stage: publish-snapshot
  tags: [ "trixnity-mac-internal" ]
  cache:
    - *pull-cache-mac
  rules:
    - *if-main
  script:
    - ./gradlew
      publishIosArm64PublicationToSnapshotRepository
      publishIosSimulatorArm64PublicationToSnapshotRepository
      publishIosX64PublicationToSnapshotRepository
      publishMacosArm64PublicationToSnapshotRepository
      publishMacosX64PublicationToSnapshotRepository
      --stacktrace

publish-snapshot:linux:
  stage: publish-snapshot
  tags: [ "saas-linux-medium-amd64" ]
  image: $BUILD_LINUX_IMAGE_NAME
  cache:
    - *pull-cache-linux
  rules:
    - *if-main
  script:
    - ./gradlew
      publishKotlinMultiplatformPublicationToSnapshotRepository
      publishJvmPublicationToSnapshotRepository
      publishAndroidReleasePublicationToSnapshotRepository
      publishJsPublicationToSnapshotRepository
      publishLinuxX64PublicationToSnapshotRepository
      publishMingwX64PublicationToSnapshotRepository
      --stacktrace

publish:create-nexus-staging-repo:
  stage: publish
  tags: [ "saas-linux-small-amd64" ]
  image: ubuntu:latest
  rules:
    - *if-release
  artifacts:
    paths: [ "./OSSRH_REPOSITORY_ID" ]
  script:
    - apt-get update && apt-get install -y curl jq
    - >
      jsonOutput=$(curl -s
      --request POST -u "$OSSRH_USERNAME:$OSSRH_PASSWORD"
      --url https://oss.sonatype.org/service/local/staging/profiles/65b35e28422b2/start
      --header 'Accept: application/json'
      --header 'Content-Type: application/json'
      --data '{ "data": {"description" : "'"$CI_COMMIT_TAG"'"} }'
      )
    - OSSRH_REPOSITORY_ID=$(echo "$jsonOutput" | jq -r '.data.stagedRepositoryId')
    - >
      if [ -z "$OSSRH_REPOSITORY_ID" ]; then
      echo "Error while creating the staging repository. Response: $jsonOutput";
      exit 1;
      fi
    - echo -n $OSSRH_REPOSITORY_ID > ./OSSRH_REPOSITORY_ID

publish:mac:
  stage: publish
  needs: [ "publish:create-nexus-staging-repo" ]
  tags: [ "trixnity-mac-internal" ]
  cache:
    - *pull-cache-mac
  rules:
    - *if-release
  script:
    - export OSSRH_REPOSITORY_ID=$(cat ./OSSRH_REPOSITORY_ID)
    - ./gradlew
      publishIosArm64PublicationToReleaseRepository
      publishIosSimulatorArm64PublicationToReleaseRepository
      publishIosX64PublicationToReleaseRepository
      publishMacosArm64PublicationToReleaseRepository
      publishMacosX64PublicationToReleaseRepository
      --stacktrace

publish:linux:
  stage: publish
  needs: [ "publish:create-nexus-staging-repo" ]
  tags: [ "saas-linux-medium-amd64" ]
  image: $BUILD_LINUX_IMAGE_NAME
  cache:
    - *pull-cache-linux
  rules:
    - *if-release
  script:
    - export OSSRH_REPOSITORY_ID=$(cat ./OSSRH_REPOSITORY_ID)
    - ./gradlew
      publishKotlinMultiplatformPublicationToReleaseRepository
      publishJvmPublicationToReleaseRepository
      publishAndroidReleasePublicationToReleaseRepository
      publishJsPublicationToReleaseRepository
      publishLinuxX64PublicationToReleaseRepository
      publishMingwX64PublicationToReleaseRepository
      --stacktrace
